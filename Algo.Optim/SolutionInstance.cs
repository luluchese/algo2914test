﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo.Optim
{
    public abstract class SolutionInstance
    {
        readonly SolutionSpace _space;
        readonly int[] _vector;
        double _cost;

        protected SolutionInstance( SolutionSpace space, int[] vector )
        {
            _space = space;
            _cost = -1.0;
            _vector = vector;
        }

        public IReadOnlyList<int> Vector 
        {
            get { return _vector; } 
        }

        public SolutionSpace Space
        {
            get { return _space; }
        }

        public double Cost 
        { 
            get 
            {
                if( _cost < 0 ) _cost = ComputeCost();
                return _cost;
            } 
        }

        protected abstract double ComputeCost();
    }
}
