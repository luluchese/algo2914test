﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo.Optim
{
    public class MeetingInstance : SolutionInstance
    {
        internal MeetingInstance( MeetingSpace space, int[] vector )
            : base( space, vector )
        {
        }

        public new MeetingSpace Space { get { return (MeetingSpace)base.Space; } }

        protected override double ComputeCost()
        {
            double totalFlightCost = 0;
            DateTime startBus = DateTime.MinValue;
            for( int iGuest = 0; iGuest < Space.Guests.Length; ++iGuest )
            {
                SimpleFlight fArrival = ArrivalFlightFor( iGuest );
                SimpleFlight fDeparture = DepartureFlightFor( iGuest );
                totalFlightCost += fArrival.Price + fDeparture.Price;
                if( fArrival.ArrivalTime > startBus ) startBus = fArrival.ArrivalTime;
            }
            // Compute waiting times.
            TimeSpan[] waitingTimes = new TimeSpan[Space.Guests.Length];
            for( int iGuest = 0; iGuest < Space.Guests.Length; ++iGuest )
            {
                SimpleFlight fArrival = ArrivalFlightFor( iGuest );
                waitingTimes[iGuest] = startBus - fArrival.ArrivalTime;
                waitingTimes[iGuest] += DepartureFlightFor( iGuest ).DepartureTime - Space.EndMeeting.AddHours( 1 );
            }
            double waitingCost = 0.0;
            waitingCost += waitingTimes.Select( w => w.TotalMinutes ).Sum() * 0.01;
            double wt = waitingTimes.Max().TotalMinutes / 90;
            waitingCost += (wt*wt) * 90;

            return totalFlightCost + waitingCost * 1.2;
        }

        private SimpleFlight DepartureFlightFor( int iGuest )
        {
            return Space.DepartureFlightsFor( iGuest )[Vector[iGuest + 9]];
        }

        private SimpleFlight ArrivalFlightFor( int iGuest )
        {
            return Space.ArrivalFlightsFor( iGuest )[Vector[iGuest]];
        }
    }
}
