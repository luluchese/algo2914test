﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;

namespace Algo.Tests
{
    [TestFixture]
    public class Reco
    {
        static string _badDataPath = Path.Combine(TestHelper.SolutionFolder, @"ThirdParty\MovieData\MovieLens\");
        static string _goodDataPath = Path.Combine(TestHelper.SolutionFolder, @"ThirdParty\MovieData\");
        static string _littleData = Path.Combine(TestHelper.SolutionFolder, @"ThirdParty\LittleMovieData\");

        [Test]
        public void CorrectData()
        {
            Dictionary<int, Movie> firstMovies;
            Dictionary<int, List<Movie>> duplicateMovies;
            Movie.ReadMovies(Path.Combine(_badDataPath, "movies.dat"), out firstMovies, out duplicateMovies);
            int idMovieMin = firstMovies.Keys.Min();
            int idMovieMax = firstMovies.Keys.Max();
            Console.WriteLine("{3} Movies from {0} to {1}, {2} duplicates.", idMovieMin, idMovieMax, duplicateMovies.Count, firstMovies.Count);

            Dictionary<int, User> firstUsers;
            Dictionary<int, List<User>> duplicateUsers;
            User.ReadUsers(Path.Combine(_badDataPath, "users.dat"), out firstUsers, out duplicateUsers);
            int idUserMin = firstUsers.Keys.Min();
            int idUserMax = firstUsers.Keys.Max();
            Console.WriteLine("{3} Users from {0} to {1}, {2} duplicates.", idUserMin, idUserMax, duplicateUsers.Count, firstUsers.Count);

            Dictionary<int, string> badLines;
            int nbRating = User.ReadRatings(Path.Combine(_badDataPath, "ratings.dat"), firstUsers, firstMovies, out badLines);
            Console.WriteLine("{0} Ratings: {1} bad lines.", nbRating, badLines.Count);

            Directory.CreateDirectory(_goodDataPath);
            // Saves Movies
            using (TextWriter w = File.CreateText(Path.Combine(_goodDataPath, "movies.dat")))
            {
                int idMovie = 0;
                foreach (Movie m in firstMovies.Values)
                {
                    m.MovieID = ++idMovie;
                    w.WriteLine("{0}::{1}::{2}", m.MovieID, m.Title, String.Join("|", m.Categories));
                }
            }

            // Saves Users
            string[] occupations = new string[]{
                "other", 
                "academic/educator", 
                "artist", 
                "clerical/admin",
                "college/grad student",
                "customer service",
                "doctor/health care",
                "executive/managerial",
                "farmer",
                "homemaker",
                "K-12 student",
                "lawyer",
                "programmer",
                "retired",
                "sales/marketing",
                "scientist",
                "self-employed",
                "technician/engineer",
                "tradesman/craftsman",
                "unemployed",
                "writer" };
            using (TextWriter w = File.CreateText(Path.Combine(_goodDataPath, "users.dat")))
            {
                int idUser = 0;
                foreach (User u in firstUsers.Values)
                {
                    u.UserID = ++idUser;
                    string occupation;
                    int idOccupation;
                    if (int.TryParse(u.Occupation, out idOccupation)
                        && idOccupation >= 0
                        && idOccupation < occupations.Length)
                    {
                        occupation = occupations[idOccupation];
                    }
                    else occupation = occupations[0];
                    w.WriteLine("{0}::{1}::{2}::{3}::{4}", u.UserID, u.Male ? 'M' : 'F', u.Age, occupation, "US-" + u.ZipCode);
                }
            }
            // Saves Rating
            using (TextWriter w = File.CreateText(Path.Combine(_goodDataPath, "ratings.dat")))
            {
                foreach (User u in firstUsers.Values)
                {
                    foreach (var r in u.Ratings)
                    {
                        w.WriteLine("{0}::{1}::{2}", u.UserID, r.Key.MovieID, r.Value);
                    }
                }
            }
        }

        [Test]
        public void ReadMovieData()
        {
            RecoContext c = new RecoContext();
            c.LoadFrom(_goodDataPath);
            for (int i = 0; i < c.Users.Length; ++i)
                Assert.That(c.Users[i].UserID, Is.EqualTo(i + 1));
            for (int i = 0; i < c.Movies.Length; ++i)
                Assert.That(c.Movies[i].MovieID, Is.EqualTo(i + 1));
        }

        [Test]
        public void TestEuclidianDistance()
        {
            var c = new RecoContext();
            c.LoadFrom(_goodDataPath);
            var u1 = c.Users[0];
            var u2 = c.Users[1];
            var u3 = c.Users[2];
            Assert.That(User.EuclidianDistance(u1, u1), Is.EqualTo(0));
            Assert.That(User.EuclidianDistance(u2, u2), Is.EqualTo(0));
            Assert.That(User.EuclidianDistance(u3, u3), Is.EqualTo(0));
        }

        [Test]
        public void TestAllPearsonSimilarity()
        {
            var c = new RecoContext();
            c.LoadFrom(_goodDataPath);
            foreach (var u1 in c.Users)
            {
                foreach (var u2 in c.Users)
                {
                    double s = c.SimilarityPearson(u1, u2);
                    Assert.That(!Double.IsNaN(s));
                    Assert.That(s >= -1 && s <= 1);
                    //Assert.That( u1 != u2 || s == 1, "u1 == u2 => s ==1" );
                    Assert.That(s == c.SimilarityPearson(u2, u1));
                }
            }
        }


        [Test]
        public void TestNumberUsers()
        {
            var c = new RecoContext();
            c.LoadFrom(_littleData);
            
            //the user has started to index 0 for the algo work
            //Never mind some stupid bug (When the user has the same note everywhere)
            int nbUser = c.Users.Count();
                        
            Assert.That(nbUser, Is.EqualTo(5));
            //Console.WriteLine(nbUser);
        }

        [Test]
        public void TestAllPearsonSimilarityWithLittleMovieData()
        {
            var c = new RecoContext();
            c.LoadFrom(_littleData);
            foreach (var u1 in c.Users)
            {
                //Initialize variables to bestkeeper it work better that way
                IReadOnlyList<BestFUser> bestFirends = u1.GetBestFUsers(c);

                foreach (var friend in bestFirends)
                {
                    double s = friend.Similarity;
                    Assert.That(!Double.IsNaN(s));
                    Assert.That(s >= -1 && s <= 1);
                    //Console.WriteLine("user " + u1.UserID + " AND user " + friend.FUser.UserID + " = " + friend.Similarity);
                }
            }
        }

        [Test]
        public void TestPearsonSimilarityConsistencyWithLittleMovieData()
        {
            var c = new RecoContext();
            c.LoadFrom(_littleData);

            var user1 = c.Users[0];
            var user2 = c.Users[1];

            //Number of ratings for user 1 and user 2
            int nbRatingUser1 = user1.Ratings.Count;
            int nbRatingUser2 = user2.Ratings.Count;
            
            Assert.That(nbRatingUser1, Is.EqualTo(10));
            Assert.That(nbRatingUser1, Is.EqualTo(nbRatingUser2));
            //Console.WriteLine(nbRatingUser1 + " " + nbRatingUser2);


            double similarityEqual1 = c.SimilarityPearson(user1, user2);

            //User 0 and user 1 are the same
            Assert.That(similarityEqual1, Is.EqualTo(1));
            //Console.WriteLine("User 1 and 2 = " + similarityEqual1);  

            var user3 = c.Users[3];
            var user4 = c.Users[4];

            double similarityEqualN1 = c.SimilarityPearson(user3, user4);

            //User 3 and 4 are oposite
            Assert.That(similarityEqualN1, Is.EqualTo(-1));
            //Console.WriteLine("User 3 and 4 = " + similarityEqualN1);
        }
    }
}
